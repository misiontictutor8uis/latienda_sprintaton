from django.shortcuts import redirect, render

from principal.models import Producto

# Create your views here.

def productos(request):

    lista_de_productos = Producto.objects.all()
    #Producto.objects.get(id=2).delete()
    contexto = { 
            "lista" : lista_de_productos
        }

    return render(request, "productos.html", contexto )

def home(request):

    if request.method=="GET":
        return render(request, "home.html" )
    else:
        nombref = request.POST["nombre"]
        cantidadf = request.POST["cantidad"]
        preciof = request.POST["precio"]
        categoriaf = request.POST["categoria"]
        Producto.objects.create(nombre=nombref, 
        cantidad=cantidadf, precio=preciof,
        categoria=categoriaf)
        return redirect('productos')

def eliminar(request, id):

    Producto.objects.get(pk=id).delete()

    return redirect('productos')

def modificar(request, id):

    if request.method=="GET":
        p = Producto.objects.get(pk=id)
        contexto = { 
            "producto" : p
        }

        return render(request, "modificar.html", contexto )
    else:
        nombref = request.POST["nombre"]
        cantidadf = request.POST["cantidad"]
        preciof = request.POST["precio"]
        categoriaf = request.POST["categoria"]
        
        prod = Producto.objects.get(pk=id)
        
        prod.nombre = nombref
        prod.cantidad = cantidadf
        prod.precio = preciof
        prod.categoria = categoriaf
        
        prod.save()
        
        return redirect('productos')