from django.db import models

# Create your models here.

class Producto(models.Model):

    nombre = models.CharField( max_length=30 )
    cantidad = models.IntegerField()
    precio = models.FloatField()
    categoria = models.CharField(max_length=20, null=True )

    def __str__(self):
        return  self.nombre
